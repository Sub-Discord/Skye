package com.Skye.listeners;

import java.util.Random;

import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class GamesThings extends ListenerAdapter {

	@Override
	public void onMessageReceived(MessageReceivedEvent e) {
		TextChannel TextChannel = e.getTextChannel();
		String Message = e.getMessage().getContent().toLowerCase();
		Random ran = new Random();
		int r = ran.nextInt(10);
				
		if (Message.startsWith("rate")) {
			String Arg = e.getMessage().getContent().toString().replaceAll("rate " , "").replaceAll("Rate " , "").replaceAll("@" , "");
			TextChannel.sendMessage("I rate " + Arg + " " + r + "/10").queue();
		}
		
		if (Message.startsWith("roll")) {
			int Rollresult = ran.nextInt(6);
			TextChannel.sendMessage("Ya rolled a " + Rollresult).queue();
		}
	}
}