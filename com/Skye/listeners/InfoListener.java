package com.Skye.listeners;

import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class InfoListener extends ListenerAdapter {

	@Override
	public void onMessageReceived(MessageReceivedEvent e) {
		TextChannel TextChannel = e.getTextChannel();
		String Message = e.getMessage().getContent().toLowerCase();
		
		if(Message.contentEquals("invite")){
			TextChannel.sendMessage("You can invite Ara here!: http://sub.swdteam.com/skye").queue();			
		}
		
		if(Message.contentEquals("ping")){
			long Ping = e.getJDA().getPing();
			TextChannel.sendMessage("Ping: " + Ping).queue();			
		}
		
		if(Message.contentEquals("group icon pls?")){
			String Avat = e.getGroup().getIconUrl();
			TextChannel.sendMessage(Avat).queue();	
		}
		
	}
}