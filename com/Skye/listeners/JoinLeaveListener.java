package com.Skye.listeners;

import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class JoinLeaveListener extends ListenerAdapter {

	@Override
	public void onGuildMemberJoin(GuildMemberJoinEvent e) {
		String JoinName = e.getMember().getUser().getName();		
		e.getGuild().getPublicChannel().sendMessage(JoinName + " has joined ").queue();
	}

	@Override
	public void onGuildMemberLeave(GuildMemberLeaveEvent e) {
		String LeaveName = e.getMember().getUser().getName();	
		e.getGuild().getPublicChannel().sendMessage(LeaveName + " has left..").queue();
	}
}
