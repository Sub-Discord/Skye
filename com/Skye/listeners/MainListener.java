package com.Skye.listeners;

import net.dv8tion.jda.client.events.relationship.FriendAddedEvent;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class MainListener extends ListenerAdapter {
	
	@Override
	public void onFriendAdded(FriendAddedEvent event) {
		
	}
	
	
	int counter = 0;

	@SuppressWarnings("deprecation")
	@Override
	public void onMessageReceived(MessageReceivedEvent e) {
		String Sub = e.getMessage().getContent().toLowerCase();

		if(e.getChannelType() == ChannelType.TEXT){
		// Test
			if (Sub.contains("arabot..?") ) {
				e.getTextChannel().sendTyping();
				e.getTextChannel().sendMessage("Yeah...?").queue();
			}

		// Help Command
		if (Sub.contains("+help") ) {
			e.getTextChannel().sendTyping().queue();
			e.getTextChannel().sendMessage(e.getAuthor().getName() + ", I've sent you a DM, hopefully..").queue();
			if(!e.getAuthor().hasPrivateChannel()){
				System.out.println("scream");
				e.getAuthor().openPrivateChannel().queue();
			}
			e.getAuthor().getPrivateChannel().sendMessage("uhhhhhhhhhhh, you see uh, this is awkward, I currently have no idea how to help you....").queue();
		}


		// Avatars..
		if (Sub.contains("avatar pls")) {
			e.getTextChannel().sendTyping();
			e.getTextChannel().sendMessage("I'll write a better way to send these later..." + "https://cdn.discordapp.com/avatars/" + e.getAuthor().getId() + "/" + e.getAuthor().getAvatarId()+ (e.getAuthor().getAvatarId().startsWith("a_") ? ".gif" : ".png")).queue();
		}

		// Kim adoption
		if (Sub.contains("adopt me") && e.getAuthor().getId() == "241559521770733568") {
			counter = counter + 1;
			if (counter == 5) {
				counter = 0;
				e.getTextChannel().sendTyping();
				e.getTextChannel().sendMessage("I think you should let her adopt you at this point..").queue();
			}
		}

		if (Sub.contains("spekkal spoder")) {
			e.getTextChannel().sendTyping();
			e.getTextChannel().sendMessage("https://www.youtube.com/watch?v=TxtvogVepSg").queue();
		}
		
		if (Sub.contains("50ap5ud5")) {
			e.getTextChannel().sendTyping();
			e.getTextChannel().sendMessage("i smell md5 encryption...").queue();
		}
		
		if (Sub.contains("whats so funny??")) {
			e.getTextChannel().sendTyping();
			e.getTextChannel().sendMessage("Your face kiddo").queue();
		}
		
		if (Sub.contains("skye twix pls") && e.getAuthor().getId() == "221367078865731584") {
			e.getTextChannel().sendTyping();
			e.getTextChannel().sendMessage("Okiiii **Gives " + e.getAuthor().getName() + " a twix**").queue();
		}
			
		
		if (Sub.contains("when did i join discord?")) {
			String Month = e.getAuthor().getCreationTime().getMonth().toString();
			int Year = e.getAuthor().getCreationTime().getYear();
			int Day = e.getAuthor().getCreationTime().getDayOfMonth();
			e.getTextChannel().sendTyping();
			e.getTextChannel().sendMessage("You joined Discord on: " + Month + " " + Day + " " + Year).queue();
		}

	 }
	}
}
